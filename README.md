
ALGORITHM V3 included
-------------------------

1. main.cpp is accomodated to ALgorithm V3
2. Algorithm_V4 usage in main.cpp is commented out


In Case of Error ( Header Files not Found )
-------------------------------------------

We need to add path of source directory

1. Project Properties > C/C++ General > Preprocessor Includes > Select GNU C++ > Select CDT User Settings Entries > Press Add and select the directory folder.
2. Project Properties > C/C++ General > Paths and Symbols > GNU C++ > Add and and select the directory folder.


In Case of lboost_python not found and lpython2.7 not found
-----------------------------------------------------------

Boost and Python is not required in this project.

1. Project Properties > C/C++ General > Paths and Symbols > Libraries > remove boost_python and python2.7.