


/*
 * trigger.cc
 *
 *  Created on: May 27, 2017
 *      Author: Saurabh
*/

#include <trillBPP/MTFSK/trigger.h>
//#include <cstdlib>
#define Fs 44100
#define G .55
#define H 1.1


int getIndexForFreq(int freq, int len, int fs) {
	int index = ( ( len / (float) fs ) * freq);
	return index;
}

vec getFilterdAmplitude(vec amp, int size) {
	int sizeAmp = amp.size();
	int freqIndex14k = getIndexForFreq(14000, size, Fs);
	vec zeroArray = amp.get(0, freqIndex14k-1);
	zeroArray.zeros();
	vec out = concat(zeroArray, amp.get(freqIndex14k, sizeAmp-1));
	for(int i=0;i<out.size();i++)
	{
		cout << out[i] << " ";
	}
	cout <<endl;
	return concat(zeroArray, amp.get(freqIndex14k, sizeAmp-1));
}

double getAverage(vec Vec, int start, int end) {
	double avg;
	double size = end - start;
	for (int i = start; i<end; i++) {
		avg += Vec[i];
	}
	return avg / size;
}

vec createChunks(vec amp, int chunk_length) {

	vec chunkedArray;
	int counter = 0;
	for (int i=0; i<amp.size(); i = i + chunk_length){
		double chunkSelect = getAverage(amp, i, (i+chunk_length));
		chunkedArray.ins(counter, chunkSelect);
		counter = counter + 1;
	}

	return chunkedArray;
}

// @todo Diversion to Deviation.
vec getDiversionSquare(vec amp, double mean, int Index14K) {
	vec diversionSquare;
	int counter = 0;

	for (int i=Index14K; i<amp.size(); i++) {
		double dataSelected = amp[i]-mean;
		dataSelected = dataSelected * dataSelected;
		diversionSquare.ins(counter, dataSelected);
		counter = counter + 1;
	}

	return diversionSquare;
}

std::pair<double, double> getGammaValue(vec diversionValuesSquare, double mean) {

	double sigmaSq = getAverage(
			diversionValuesSquare,
			0,
			diversionValuesSquare.size()
	);

	double sigma = pow(sigmaSq, 0.5);
	double gamma = ( (G * sigmaSq) + (H * sigma) ) / mean;

	return make_pair(gamma, sigmaSq);

}

std::pair<double, double> checkCondition(vec amp, double gamma, double mean){
	int sizeAmp = amp.size();

	int freq14KIndex = getIndexForFreq(15000, sizeAmp, 22050) + 1;
	int freq16KIndex1 = getIndexForFreq(16000, sizeAmp, 22050) + 1;

	int freq16KIndex2 = getIndexForFreq(16500, sizeAmp, 22050) + 1;
	int freq18KIndex = getIndexForFreq(17500, sizeAmp, 22050) + 1;

	double gamma14K = 0.1;
	int counter14 = 0;
	double gamma16K = 0.1;
	int counter16 = 0;
	vec top100_15 = amp.get(freq14KIndex,freq16KIndex1);
	for( int i =freq14KIndex; i < freq18KIndex; i++) {
		if(i < freq16KIndex1 ) {
			if (abs(mean - amp[i]) > gamma) {
				gamma14K += amp[i];
				counter14 += 1;

			}
		}
		else if( i > freq16KIndex2) {
			if (abs(mean - amp[i]) > gamma) {
				gamma16K += amp[i];
				counter16 += 1;
			}
		}
	}

	if (gamma14K > 0.1) {
		gamma14K = gamma14K / counter14;
	}
	if (gamma16K > 0.1) {
		gamma16K = gamma16K / counter16;
	}

	double ratio = gamma16K / gamma14K;
	return make_pair(gamma14K, gamma16K);
}

float sumVector(vec data, int starting_point, int ending_point) {
    float result = 0;
    for (int i=starting_point; i < ending_point; i++) {
        result += (data[i]*data[i]); //Amplitude square for getting Power
    }
    return result;
}


double Trigger::isFound(cvec input, bool solo_correlation) {
	if(solo_correlation==true)
		input = fft(input);
	vec mag = abs(input); 		//Note : The input cvec is Computed FFT.
	int data_size = mag.size();
	vec amp = mag.get(0, (data_size/2)-1); // discarding mirror image

	vec filteredArray = getFilterdAmplitude(amp, data_size); // get amplitude between 14k to 22050.
	vec chunkedData = filteredArray;
	int freq15Kindex = getIndexForFreq(15000, chunkedData.size(), Fs/2); // find index of 14000
	double mean = getAverage(
			chunkedData,
			freq15Kindex,
			chunkedData.size()
	);
	vec diversionValuesSquare = getDiversionSquare(chunkedData, mean, freq15Kindex);
    std::pair<double, double> resultGamma = getGammaValue(diversionValuesSquare, mean);
	double gammaValue = resultGamma.first;
	double sigmaSqrValue = resultGamma.second;
	std::pair<double, double> result = checkCondition(chunkedData, gammaValue, mean);
    double sigma = pow(sigmaSqrValue, 0.5);
    double gamma_result = result.second/result.first;
    return gamma_result;
}






/*
//// NEW TRIGGER LOGIC BASED ON POWER OF THE SIGNAL

 * trigger.cc
 *
 *  Created on: May 27, 2017
 *      Author: Saurabh

#include <trillBPP/MTFSK/trigger.h>
//#include <cstdlib>
#define Fs 44100


int getIndexForFreq(int freq, int len, int fs) {
	int index = ( ( len / (float) fs ) * freq);
	return index;
}

vec getFilterdAmplitude(vec amp, int size) {
	int sizeAmp = amp.size();
	int freqIndex14k = getIndexForFreq(14000, size, Fs);
	vec zeroArray = amp.get(0, freqIndex14k-1);
	zeroArray.zeros();
	vec out = concat(zeroArray, amp.get(freqIndex14k, sizeAmp-1));
	for(int i=0;i<out.size();i++)
	{
		cout << out[i] << " ";
	}
	cout <<endl;
	return concat(zeroArray, amp.get(freqIndex14k, sizeAmp-1));
}

double GetSqrSum(vec Array)
{
	double result =0;
	for(int i=0;i<Array.size();i++)
	{
		result = result + (Array[i]*Array[i]);
	}
	result = result / Array.size();
	return result;
}

double Trigger::isFound(cvec input, bool solo_correlation) {
	if(solo_correlation==true)
		input = fft(input);
	vec mag = abs(input); 		//Note : The input cvec is Computed FFT.
	int data_size = mag.size();
	vec amp = mag.get(0, (data_size/2)-1); // discarding mirror image

	vec filteredArray = getFilterdAmplitude(amp, data_size); // get amplitude between 14k to 22050.
	vec chunkedData = filteredArray;
	int freq14Kindex = getIndexForFreq(14000, chunkedData.size(), Fs/2); // find index of 14000
	int freq16Kindex = getIndexForFreq(16000, chunkedData.size(), Fs/2); // find index of 16000
	int freq16_5Kindex = getIndexForFreq(16500, chunkedData.size(), Fs/2); // find index of 16500
	int freq18_5Kindex = getIndexForFreq(18500, chunkedData.size(), Fs/2); // find index of 18500
	vec amp14_16K = amp.get(freq14Kindex,freq16Kindex);
	vec amp16_18K = amp.get(freq16_5Kindex,freq18_5Kindex);
	double Sum_Sqr_Avg_14K,Sum_Sqr_Avg_16K;
	Sum_Sqr_Avg_14K = GetSqrSum(amp14_16K);
	Sum_Sqr_Avg_16K = GetSqrSum(amp16_18K);
	double gamma_result = Sum_Sqr_Avg_16K/Sum_Sqr_Avg_14K;
    return gamma_result;
}
*/








