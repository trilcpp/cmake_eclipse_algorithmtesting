/*
 * main.cpp
 *
 *  Created on: 10-jan-2019
 *      Author: narasimha vineeth
 */



/////////PROGRAM WRITTEN USING LIBSNDFILE TO OPEN SOUND FILES
////DONT FORGET TO INSTALL AND INCLUDE THE LIBRARY IN ECLIPSE PROPERTIES OF PROJECT

#include "fftw3.h"
#include <trillBPP/vector/vec.h>
#include <trillBPP/MTFSK/modulation.h>
#include <trillBPP/comm/reedsolomon.h>

#include <iostream>
#include <iterator> //for ostream_iterator
#include <stdio.h>
#include <stdlib.h>
#include <sndfile.h>
#include <vector>
#include <sys/time.h>
#include <fstream>	//For JSON file
#include <jsoncpp/json/json.h>
#include <MTFSK/MTFSK.h>
#include <MTFSK/finddata.h>
#include <MTFSK/modulation.h>
#include <MTFSK/modulation.h>
#include <MTFSK/trigger.h>
//#include<android/log.h>

#include <trillBPP/miscfunc.h>

//#define file_path "/home/shivujagga/Testing files/2Peaks/testfiles/default_37.wav"
//---------- Android
//#define file_path "/home/shivujagga/Softwares/Server/testingframework/2peakTest/audio/default_5.wav"
//---------- Iphone
//#define file_path "/home/shivujagga/Softwares/Server/testingframework/Android/audio_test_1024_2048/2048/0_meter38.wav"

//#define file_path "/home/vineeth/Music/Fsk_mod_demod/recording/72bitswiththreechecksumsand4reputation.wav"					  //For RLS
#define text_file_path "/home/vineeth/eclipse-workspace/cmake_eclipse_algorithmtesting/data_raw_bpsk.json" //For RLS test
//#define file_path "/home/vineeth/Downloads/1_10.wav"					  //For RLS
//#define file_path "/home/vineeth/Music/Fsk_mod_demod/recording/310bitswithreedsolomonencoder.wav"
#define file_path "/home/vineeth/Downloads/data_8_Mfsk_tb_156_bits.wav"
//#define file_path "/home/vineeth/Downloads/Hello_155193638171.wav"
//#define file_path "/home/vineeth/Downloads/2m_reed_noshake.wav"
//#define file_path "/home/vineeth/Downloads/6-4-walk-390.wav"
//#define file_path "/home/vineeth/Downloads/default_110.wav"
//#define file_path "/home/vineeth/Downloads/data_8_Mfsk_tb50_per_overlap_fc_17500guard_band_150.wav"
using namespace std;
using namespace trill;

#define SEPARATION 28000
#define TOTAL_CHUNKS 3

void Plot(vec &input)
{
	FILE* gnuplot_pipe = popen ("gnuplot -persistent", "w");
	        // basic settings
	        fprintf(gnuplot_pipe, "set title '%s'\n", "Correlation Output");
	        // fill it with data
	        fprintf(gnuplot_pipe, "plot '-'\n");
	        for(int j=0; j<input.size(); ++j){
	            fprintf(gnuplot_pipe, "%d %f\n", j, input[j]);
	        }
	        fprintf(gnuplot_pipe, "e\n");
	        fflush(gnuplot_pipe);
	}
int main(){
	cout<<"--------------Algorithm Welcomes you a lot:"<<endl;

/*----Libsnd  Read file*/
	//char        *infilename;
	SNDFILE     *file = NULL;
	SF_INFO     sfinfo;
	//int num_channels;
	//int num;
	int  num_items;
	int *buf;
	int frame, samplerate, ch;
	//int i, j;
	//FILE        *outfile = NULL;
	//Read the file, into buffer
	file = sf_open(file_path, SFM_READ, &sfinfo);
	 //Print some of the info, and figure out how much data to read.
	frame = sfinfo.frames;
	samplerate = sfinfo.samplerate;
	ch = sfinfo.channels;
	printf("frames=%d\n", frame);
	printf("samplerate=%d\n", samplerate);
	printf("channels=%d\n", ch);
	num_items = frame * ch;
	printf("num_items=%d\n", num_items);
	//Allocate space for the data to be read, then read it
	buf = (int *)malloc(num_items * sizeof(int));
	//num = sf_read_int(file, buf, num_items);
	sf_count_t data;
	double* p;
	p = (double *)malloc(sizeof(double)*num_items);
	data = sf_readf_double(file,p,frame);
	//printf("\n");
	sf_close(file);
	ifstream text_file1(text_file_path);
	Json::Reader reader1;
	Json::Value obj1;
	reader1.parse(text_file1, obj1);     // Reader can also read strings
	vec JSON_input = obj1["data_raw"].asString();
	std::stringstream result;
	vec inputfinal1(num_items);
	for(long long int i=0;i<num_items;i++)
	{
		inputfinal1[i]=p[i];
	}
	std::copy(buf, buf+num_items, std::ostream_iterator<double>(result, ","));
	vec input = result.str();
    int no_of_bits = 156;
	int no_for_peak = 20000;
	int ChunkSize = 4096;
	double peak_ratio=0.45;
	MTFSK bP;
	clock_t start = clock();
	//cout << "MTFSK STARTED " << endl;
	int micstate =0;
	vec corrout;
	//cout << inputfinal1.size() << endl;
	//Plot(inputfinal1);
	//cout << inputfinal1 << endl;
	float inputFlo[inputfinal1.size()];
	for(int i=0;i<inputfinal1.size();i++)
	{
		inputFlo[i] = inputfinal1[i];
	}
	FILE *fp;
	fp=fopen("test_phonenumber_vineeth.txt", "w");
	fwrite(inputFlo, sizeof(float), inputfinal1.size(), fp);
	vec answer = bP.trillDecoder_MTFSK_8(inputfinal1,no_of_bits,no_for_peak, ChunkSize,peak_ratio,0);
    //cout << "Time taken for complete demod" << (double)(clock() - start)/CLOCKS_PER_SEC;
    vec reedoutput = "-1 -1 -1 -1 1 1 1 -1 -1 -1 1 -1 -1 1 1 -1 -1 1 -1 -1 -1 1 1 -1 -1 1 1 1 -1 -1 -1 -1 -1 1 1 1 1 1 1 1 1 -1 1 1 1 1 -1 -1 1 1 1 -1 -1 1 -1 1 -1 -1 1 1 -1 -1 1 -1 -1 -1 1 1 -1 1 1 -1 -1 -1 1 -1 -1 -1 -1 1 1 -1 1 1 1 -1 -1 -1 -1 -1 1 -1 -1 1 1 1 1 -1 1 -1 -1 1 -1 1 -1 -1 -1 1 1 -1 -1 1 -1 -1 1 -1 -1 -1 1 1 1 1 1 -1 1 1 1 -1 -1 1 -1 1 -1 1 -1 -1 1 1 -1 1 -1 -1 -1 -1 -1 -1 1 -1 1 1 1 1 -1 -1 1 -1 -1 1 -1 -1 -1 1 -1 1 1 1 1 1 1 -1 1 1 -1 1 1 1 1 -1 1 -1 -1 1 1 1 -1 1 1 -1 -1 -1 1 1 -1 -1 -1 -1 1 1 -1 -1 1 -1 1 1 1 -1 1 1 1 1 -1 -1 1 1 -1 1 -1 1 -1 1 1 -1 -1 -1 1 1 -1 1 1 1 1 -1 -1 1 1 -1 1 -1 1 -1 1 1 1 1 -1 -1 -1 1 1 1 -1 1 -1 1 1 1 1 -1 -1 -1 1 -1 -1 -1 -1 1 1 1 -1 -1 1 1 -1 1 1 1 1 -1 1 1 1 1 1 1 -1 -1 -1 1 -1 1 -1 1 1 1 -1 -1 1 -1 -1 -1 -1 1 -1 1 -1 1 1 1 1 1";
    vec hellowo = "1 1 1 1 -1 1 -1 1 1 1 -1 -1 1 1 1 1 1 -1 -1 -1 1 1 1 -1 -1 1 -1 -1 1 1 -1 -1 1 -1 -1 -1 1 1 1 -1 1 -1 1 1 -1 1 -1 1 -1 1 1 -1 1 1 -1 1 -1 -1 1 1 -1 -1 1 1 -1 -1 -1 1 1 1 -1 -1 1 1 1 1 -1 -1 -1 -1 -1 -1 1 1 -1 -1 -1 -1 1 1 -1 -1 -1 -1 -1 1 -1 1 1 1 1 -1 -1 1 1 -1 1 1 -1 1 1 -1 1 -1 -1 1 -1 -1 1 -1 -1 -1 -1 -1 -1 -1 -1 -1 1 1 -1 1 -1 -1 1 1 1 1 -1 1 -1 -1 -1 1 -1 1 1 1 -1 1 1 -1 1 -1 1 1";
    vec data_maininput = "1 -1 1 1 1 -1 -1 1 -1 1 -1 1 -1 -1 1 -1 -1 1 -1 1 1 1 -1 -1 -1 -1 -1 1 -1 1 1 1 1 -1 -1 1 -1 1 -1 -1 1 1 -1 -1 1 1 -1 1 1 -1 -1 -1 -1 1 -1 -1 1 1 -1 1 1 1 -1 1 1 1 1 -1 -1 -1 -1 -1 -1 -1 1 1 1 1 1 1 -1 1 -1 -1 1 1 1 -1 -1 1 -1 -1 1 1 -1 -1 1 -1 1 -1 1 1 -1 -1 -1 1 1 -1 1 -1 -1 -1 -1 -1 1 -1 1 1 -1 1 1 -1 -1 1 1 1 1 -1 1 1 -1 -1 -1 -1 -1 -1 -1 1 -1 -1 1 1 1 1 -1 -1 -1 -1 1 -1 1 1 -1 1 -1 -1 -1 1 -1 1 -1 -1 1 -1 -1 -1 1 -1 -1 1 1 -1 1 1 1 1 -1 -1 -1 -1 1 1 -1 -1 1 -1 -1 -1 1 -1 1 -1 1 1 1 1 1 -1 1 1 -1 1 -1 -1 -1 1 -1 -1 1 -1 -1 1 1 1 1 -1 1 1 -1 1 -1 1 -1 1 1 1 1 1 -1 -1 1 -1 -1 1 1 -1 1 1 1 -1 -1 -1 -1 -1 -1 -1 -1 -1 1 1 -1 -1 1 1 -1 1 -1 -1 -1 -1 -1 1 1 -1 -1 1 -1 -1 1 -1 1 -1 1 -1 -1 -1 1 1 -1 1 1 -1 1 1 1 -1 1 1 -1 -1 -1 1 -1 -1 -1 1 1 -1 1 1 1 -1 1 1 1 1 -1 1 1 -1 -1 1 -1 -1 1 1 1 -1 -1 1 -1 -1 -1 1 1 1 -1 1 1 1 1 1 -1 1 -1 -1 1 -1 1 -1 1 -1 1 -1 1 1 1 1 1 -1 -1 1 -1 1 1 -1 -1 1 1 1 -1 1 -1 -1 1 1 1 -1 -1 -1 1 -1 -1 1 1 -1 -1 -1 1 1 1 1 -1 -1 1 -1 1 -1 1 1 -1 -1 1 -1 1 1 1 -1 -1 1 1 -1 1 -1 -1 -1 1 -1 -1 1 1 1 1 1 1 1 -1 1 -1 -1 1 -1 1 -1 -1 1 1 -1 1 -1 -1 -1 -1 -1 -1 1 1 1 -1 -1 -1 1 1 -1 1 1 -1 1 1 1 1 -1 -1 1 -1 -1 -1 -1 -1 1 1 1 -1 1 -1 1 1 -1 1 -1 1 1 1 1 1 -1 -1 -1 1 -1 -1 1 1 1 1 1 1 -1 1 1 -1 -1 -1 -1 -1 1 1 -1 -1 1";
    vec data_complete = "-1 -1 -1 -1 1 1 1 -1 -1 -1 1 -1 -1 1 1 -1 -1 1 -1 -1 -1 1 1 -1 -1 1 1 1 -1 -1 -1 -1 -1 1 1 1 1 1 1 1 1 -1 1 1 1 1 -1 -1 1 1 1 -1 -1 1 -1 1 -1 -1 1 1 -1 -1 1 -1 -1 -1 1 1 -1 1 1 -1 -1 -1 1 -1 -1 -1 -1 1 1 -1 1 1 1 -1 -1 -1 -1 -1 1 -1 -1 1 1 1 1 -1 1 -1 -1 1 -1 1 -1 -1 -1 1 1 -1 -1 1 -1 -1 1 -1 -1 -1 1 1 1 1 1 -1 1 1 1 -1 -1 1 -1 1 -1 1 -1 -1 1 1 -1 1 -1 -1 -1 -1 -1 -1 1 -1 1 1 1 1 -1 -1 1 -1 -1 1 -1 -1 -1 1 -1 1 1 1 1 1 1 -1 1 1 -1 1 1 1 1 -1 1 -1 -1 1 1 1 -1 1 1 -1 -1 -1 1 1 -1 -1 -1 -1 1 1 -1 -1 1 -1 1 1 1 -1 1 1 1 1 -1 -1 1 1 -1 1 -1 1 -1 1 1 -1 -1 -1 1 1 -1 1 1 1 1 -1 -1 1 1 -1 1 -1 1 -1 1 1 1 1 -1 -1 -1 1 1 1 -1 1 -1 1 1 1 1 -1 -1 -1 1 -1 -1 -1 -1 1 1 1 -1 -1 1 1 -1 1 1 1 1 -1 1 1 1 1 1 1 -1 -1 -1 1 -1 1 -1 1 1 1 -1 -1 1 -1 -1 -1 -1 1 -1 1 -1 1 1 1 1 1 ";//obj["data_final_bits"].asString();
	bvec uncoded_bits = "1 0 1 1 1 0 0 1 0 1 0 1 0 0 1 0 0 1 0 1 1 1 0 0 0 0 0 1 0 1 1 1 1 0 0 1 0 1 0 0 1 1 0 0 1 1 0 1 1 0 0 0 0 1 0 0 1 1 0 1 1 1 0 1 1 1 1 0 0 0 0 0 0 0 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1";
	vec vinnuphonenumber = "1 1 -1 1 1 -1 1 -1 -1 1 -1 -1 1 -1 -1 -1 -1 -1 1 1 -1 1 1 1 -1 -1 -1 1 -1 1 -1 1 1 -1 -1 1 1 -1 -1 -1 1 1 -1 1 -1 1 -1 1 1 -1 1 1 1 1 1 -1 1 -1 1 1 -1 -1 1 1 -1 1 -1 1 1 -1 1 1 -1 1 -1 -1 -1 -1 -1 -1 1 1 1 -1 -1 -1 1 -1 -1 1 1 1 -1 1 1 -1 1 1 1 -1 1 1 1 -1 -1 -1 -1 -1 1 -1 -1 -1 -1 -1 1 1 -1 -1 -1 1 1 1 1 1 -1 -1 -1 -1 1 1 1 -1 -1 -1 -1 -1 1 -1 -1 1 -1 1 1 1 -1 -1 1 1 1 -1 -1 -1 1 1 -1 -1 -1 -1 -1 -1 1 1 -1 -1 -1 1 -1 -1 1 1 -1 -1 -1 1 -1 1 1 -1 -1 -1 -1 -1 -1 -1 -1 -1 1 1 1 -1 1 -1 1 -1 1 1 1 1 1 -1 -1 -1 1 -1 1 -1 -1 -1 1 -1 -1 -1 -1 -1 1 1 -1 1 -1 1 1 1 -1 -1 1 -1 -1 1 1 -1 -1 -1 -1 1 -1 -1 -1 -1 1 1 -1 -1 -1 1 1 -1 1 1 -1 -1 1 1 -1 1 1 1 -1 1 1 1 1 -1 1 -1 1 1 -1 -1 1 -1 -1 1 -1 1 1 -1 -1 1 1 -1 -1 -1 -1 1 1 1 1 1 1 1 -1 1 -1 1 -1 1 1 1 -1 1 1 1 -1 1 -1 1 -1 1 1 1";
	int err=0;
	cout << hellowo.size() << endl;
	for(int i=0;i<answer.size();i++)
	{
		if(answer[i] != hellowo[i])
		{
			err++;
			cout << i << ",";
		}
	}
	cout << endl;
	cout << "Total error : " << err << endl;
	return 0;
}



